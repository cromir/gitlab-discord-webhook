# Discord Gitlab Webhook

## How to use

Insert the following:

| field | value |
|-|-|
| URL | `{url_of_this_script}` |
| Secret Token | `{webhook.id}/{webhook.token}` |

## options

Options are supplied as GET parameters.
Example: `{url_of_this_script}?color=3447003&url=true`

All options listed below are optional

| option | type |description | default |
|-|-|-|-|
| color | number | sets color for the messages | `3447003` |
| url | bool | defines if a url should be added | `false` |
