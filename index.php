<?php
require_once('Parsedown.php');

define('DISCORD_SERVER','https://discordapp.com/api/webhooks/');

function post($path, $data = []) {
    $context = stream_context_create([
        'http' => [
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => 'payload_json=' . json_encode($data)
        ]
    ]);
    $url = DISCORD_SERVER . $path;
    return file_get_contents($url, false, $context);
}

function prepareData(){
  $inputJSON = file_get_contents('php://input');
  $json = json_decode($inputJSON, TRUE);
  if ($_SERVER['HTTP_X_GITLAB_EVENT'] == 'Push Hook') {
    //$data = ['content' => "{$json['user_name']} pushed new commit {$json['after']} to {$json['project']['name']}"];
    $commits = [];
    foreach($json['commits'] as $commit) {
      $commits[] = [
        'name' => $commit['id'],
        'value' => $commit['message'],
        'inline' => false,
      ];
    }
    $count = count($commits);
    $data = ['embeds' => [
      [
        'author' => [
          'name' => $json['user_name'],
          'url' => 'https://gitlab.com/' . $json['user_username'],
          'icon_url' => $json['user_avatar'],
        ],
        'color' => (isset($_GET['color']) ? $_GET['color'] : 3447003),
        'title' => 'New Commits',
        'description' => "pushed {$count} new commits to {$json['project']['web_url']}",
        'fields' => $commits,
        'provider' => [
          'url' => 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'],
          'name' => 'discord-gitlab-webhook',
        ],
      ],
    ]];
    if(isset($_GET['url']) && $_GET['url'] == 'true')
      $data['url'] = $json['commits'][0]['url'];
  }
  else {
    $data = ['content'=>'undefined method'];
  }
  return $data;
}

if(isset($_SERVER['HTTP_X_GITLAB_TOKEN'])){
  var_dump(post($_SERVER['HTTP_X_GITLAB_TOKEN'], prepareData()));
}
else{
  $Parsedown = new Parsedown();
  echo $Parsedown->text(str_replace('{url_of_this_script}', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], file_get_contents('README.md')));
}
